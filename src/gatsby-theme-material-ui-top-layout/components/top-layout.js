import React from "react";
import { ThemeProvider } from "@material-ui/core/styles";

export default function TopLayout({ children, theme }) {
    return <ThemeProvider theme={theme}>{children}</ThemeProvider>;
};
