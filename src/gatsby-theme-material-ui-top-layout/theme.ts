import { createMuiTheme, responsiveFontSizes } from "@material-ui/core/styles";

let theme = createMuiTheme({
    overrides: {
        MuiCssBaseline: {
            '@global': {
                html: {
                    fontSize: '10px',
                },
            },
        },
    },
    palette: {
        primary: { main: '#00D5FF', },
        secondary: { main: '#D700FF' },
        background: {
            default: '#131A1D',
            paper: '#1C262B',
        },
        text: {
            primary: '#fff',
        },
        divider: '#fff',
    },
    typography: { 
        htmlFontSize: 10,
        h1: { 
            fontSize: '4.8rem', 
            fontWeight: 400,
            lineHeight: 1,
        },
        h2: {
            fontSize: '3rem',
            fontWeight: 400,
            lineHeight: 1,
        },
        button: { fontSize: '1.7rem' },
        body1: { 
            fontSize: '1.5rem',
            lineHeight: 1.3,
        },
        body2: { fontSize: '2rem' },
    },
});

export default responsiveFontSizes(theme);
