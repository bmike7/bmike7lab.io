import React, { HTMLAttributes, useContext } from 'react';
import clsx from "clsx";
import { createStyles, makeStyles, Theme, lighten } from "@material-ui/core/styles";
import { Typography } from "@material-ui/core";

import Hero from '../../assets/Hero';

interface DeveloperContextProps {
    executing: boolean,
    handler?: (action: DeveloperAction) => Promise<void>,
}

export const DeveloperContext = React.createContext<DeveloperContextProps>({
    executing: false,
});

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: { position: 'relative' },
        terminal: {
            position: 'absolute',
            top: '70%',
            left: '50%',
            transform: 'translateX(-44%)',
            minWidth: '43%',
            color: 'green',
            fontSize: '2.8rem',
            borderRadius: 5,
            padding: theme.spacing(1),
            boxShadow: theme.shadows[12],
            backgroundColor: lighten(theme.palette.background.paper, .1),
        },
        grow: {
            transition: 'all .2s ease-in-out',
            transform: 'scale(1.1)',
        },
    })
);

interface TerminalProps extends HTMLAttributes<HTMLDivElement>{
    text: string,
}

const Terminal: React.FC<TerminalProps> = ({text, ...props}) => (
    <div {...props}>
        <Typography variant="body2" >{`${text}`}</Typography>
    </div>
);

export interface DeveloperAction {
    command: string,
    path: string,
    action: () => void,
}

interface DeveloperProps extends HTMLAttributes<HTMLDivElement> {
    terminalText: TerminalProps["text"],
}

const DeveloperComponent: React.FC<DeveloperProps> = ({ className, terminalText, ...props }) => {
    const classes = useStyles();
    const { executing } = useContext(DeveloperContext);

    return (
    <div className={clsx(classes.root, className, {[classes.grow]: executing})} {...props}>
        <Hero />
        {terminalText && <Terminal className={classes.terminal} text={terminalText} />}
    </div>
)};
export default DeveloperComponent;
