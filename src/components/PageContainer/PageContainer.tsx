import React from "react";
import { CssBaseline } from "@material-ui/core";

import SEO, { SEOProps } from "../SEO";

// className: so "position: 'relative'" could be set on the hole page if wanted so.
interface PageContainerProps {
    seo?: SEOProps,
    className?: HTMLDivElement["className"],
}

const PageContainer: React.FC<PageContainerProps> = ({ seo, className, children}) => (
    <div className={className}>
        <SEO {...seo}/>
        <CssBaseline />
        {children}
    </div>
);

export default PageContainer;
