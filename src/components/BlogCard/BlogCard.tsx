import React, { useContext } from "react";
import { navigate } from "gatsby";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";

import Button from "@material-ui/core/Button";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";
import { DeveloperContext } from "../Developer";

const useStyles = makeStyles((theme: Theme) => 
    createStyles({
        cardRoot: { 
            width: 300,
            minWidth: 300,
            margin: theme.spacing(2),
            boxShadow: `0px 0px 0px 1px ${theme.palette.primary.main}`,
            backgroundColor: theme.palette.background.default,
            display: "flex",
            flexDirection: "column",
            justifyContent: "space-between",
        },
        cardMedia: { height: 150 },
        cardButton: {
            position: "relative",
            bottom: 0
        }
    })
);

export interface Blog {
    id: string,
    fields: {
        slug: string,
    },
    frontmatter: {
        title: string,
        url: string,
        description: string,
        date: string,
    }
}

interface BlogCardProps {
    blog: Blog,
}

const BlogCard: React.FC<BlogCardProps> = ({ blog }) => {
    const { fields, frontmatter } = blog;
    const classes = useStyles();
    const { handler: contextHandler } = useContext(DeveloperContext);

    const handleReadMoreClick = (path: Blog["fields"]["slug"]) => {
        window.scrollTo({
            top: 0,
            behavior: "smooth",
        });
        contextHandler ? 
            contextHandler({
                command: "cd",
                path: path.slice(1),
                action: () => navigate(path),
            }) : navigate(path);
    }

    return (
        <Card className={classes.cardRoot}>
                <CardMedia
                    className={classes.cardMedia}
                    // 🙏: Florian Olivo: https://unsplash.com/photos/4hbJ-eymZ1o
                    image={frontmatter.url.length > 0 ? frontmatter.url : require("../../assets/images/defaultCoding.jpg")}
                />
                <CardContent>
                    <Typography gutterBottom variant="h5" component="h3">
                        {frontmatter.title}
                    </Typography>
                    <Typography variant="body1" component="p">
                        {frontmatter.description}
                    </Typography>
                </CardContent>
                <CardActions className={classes.cardButton}>
                    <Button onClick={() => handleReadMoreClick(fields.slug)} 
                        color="primary">
                            Read More
                    </Button>
                </CardActions>
        </Card>
    )
}

export default BlogCard;
