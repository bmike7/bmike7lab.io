import React from "react";
import clsx from "clsx";
import { Divider } from "@material-ui/core";
import { 
    createStyles,
    makeStyles,
    Theme,
} from "@material-ui/core/styles";

const useStyles = makeStyles((theme: Theme) => 
    createStyles({
        footer: {
            padding: theme.spacing(3, 0, 1),
            textAlign: 'center',
        },
        divider: {
            width: '90%',
            margin: 'auto',
        },
        span: {
            '@media (max-width: 500px)': { display: 'none' }
        },
        fullWidth: {
            '@media (max-width: 500px)': { display: 'block' },
        }
    })
);

interface FooterProps {
    className?: HTMLDivElement["className"],
}

const Footer: React.FC<FooterProps> = ({ className }) => {
    const classes = useStyles();
    return (
    <footer className={clsx(classes.footer, className)}>
        <Divider className={classes.divider} />
        <p>
            <span className={classes.fullWidth}>MIKE BIJL - Web Developer</span> 
            <span className={classes.span}>&nbsp; - &nbsp;</span>
            <span className={classes.fullWidth}>bijlmike3@gmail.com - +324 78 77 43 81</span>
        </p>
    </footer>
)};
export default Footer;
