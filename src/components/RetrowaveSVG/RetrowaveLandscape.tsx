import React, { HTMLAttributes } from "react";
import clsx from "clsx";
import {
    createStyles,
    makeStyles, 
} from "@material-ui/core/styles";

import Landscape from '../../assets/Retrowave';
import useWindowSize from "../../hooks/useWindowSize";

const useStyles = makeStyles( 
    createStyles({
        retrowave: {
            height: 0,
            overflow: 'visisble',
            position: 'relative',
        }
    })
);

const RetrowaveLandscape: React.FC<HTMLAttributes<HTMLDivElement>> = ({ className, ...props }) => {
    const classes = useStyles();  
    const svgRef = React.useRef(null);
    const [offset, setOffset] = React.useState<number>(0);
    const { width } = useWindowSize();

    React.useEffect(() => setOffset(svgRef.current.clientHeight * 2/3), [width]);

    return (
    <div className={clsx(classes.retrowave, className)} style={{ top: `-${offset}px`}} {...props}>
        <Landscape ref={svgRef} />
    </div>
)};
export default RetrowaveLandscape;
