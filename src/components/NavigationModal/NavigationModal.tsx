import React from "react";
import { navigate } from "gatsby";
import { 
    Modal, 
    ModalProps,
    Fade,
    FadeProps,
    Backdrop,
    BackdropProps,
} from "@material-ui/core";
import { createStyles, makeStyles, Theme, lighten } from "@material-ui/core/styles";

import Dot from "@material-ui/icons/FiberManualRecord";
import NavigationFolder from "../NavigationFolder";
import { DeveloperContext } from "../Developer";

const useStyles = makeStyles((theme: Theme) => 
    createStyles({
        folderContainer: { 
            width: '100%',
            display: 'flex',
            flexWrap: 'wrap',
        },
        windowTop: {
            width: '100%',
            height: '23px',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'flex-end',
            backgroundColor: lighten(theme.palette.background.paper, .1),
        },
        dot: { height: '80%' },
    })
);

interface Route {
    name: string,
    path: string,
}

const routes: Route[] = [
    { name: 'home', path: '/' },
    { name: 'testimony', path: '/testimony/' },
    { name: '404😉', path: '/themostawesomestblogpost/' },
];

interface NavigationModalProps {
    className?: HTMLDivElement["className"],
    modalProps: Omit<ModalProps, "children">,
    fadeProps?: FadeProps,
    backdropProps?: BackdropProps,
};

const NavigationModal: React.FC<NavigationModalProps> = props => {
    const { 
        className, 
        modalProps, 
        fadeProps, 
        backdropProps,
    } = props;
    const { open, onClose } = modalProps;
    const classes = useStyles();
    const { handler: contextHandler } = React.useContext(DeveloperContext);

    const handleClick = (path: string) => {
        onClose({}, "backdropClick");
        contextHandler ? 
            contextHandler({
                command: "cd",
                path: path.slice(1),
                action: () => navigate(path),
            }) : navigate(path);
    };

    return (
        <Modal 
            aria-labelledby="navigation-modal"
            aria-describedby="navigation-modal-description"
            open={open}
            closeAfterTransition
            BackdropComponent={Backdrop}
            BackdropProps={{ timeout: 500, ...backdropProps }}
            onClose={onClose}
            {...modalProps}>
                <Fade in={open} {...fadeProps}>
                    <div className={className}>
                        <div className={classes.windowTop}>
                            <Dot className={classes.dot} style={{ color: 'green' }} />
                            <Dot className={classes.dot} style={{ color: 'orange' }} />
                            <Dot className={classes.dot} style={{ color: 'red' }} />
                        </div>
                        <div className={classes.folderContainer}>
                            {routes.map((route, id) => 
                                <NavigationFolder key={id} 
                                    name={route.name} 
                                    path={route.path}
                                    handleClick={handleClick}
                                    isActive={
                                        typeof window != "undefined"
                                        && window.location.pathname == route.path
                                    }
                                />)
                            }
                        </div>
                    </div>
                </Fade>
        </Modal>
    );
};

export default NavigationModal;
