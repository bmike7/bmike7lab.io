import React, { HTMLAttributes } from "react";
import clsx from "clsx";
import Helmet from "react-helmet";
import { Typography } from "@material-ui/core";
import {
    createStyles,
    makeStyles,
    Theme,
} from "@material-ui/core/styles";
import { Button } from "gatsby-theme-material-ui"

import TypingSVG from "../../assets/Typing";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        center: {
            position: 'absolute',
            top: '50%',
            left: '50%',
            transform: 'translate(-50%, -50%)',
            textAlign: 'center',
        },
        title: { 
            fontSize: '4rem',
            marginTop: '-13%',
            textTransform: 'uppercase',
        },
        button: {
            padding: theme.spacing(.5, 3),
            marginTop: theme.spacing(2),
        }
    })
);

const WIP: React.FC<HTMLAttributes<HTMLDivElement>> = ({ className, ...props }) => {
    const classes = useStyles();
    return (
    <div className={clsx(classes.center, className)} {...props} >
        <Helmet>
            <title>Mike Bijl | Work In Progress</title>
        </Helmet>
        <TypingSVG />
        <Typography variant="h1" className={classes.title}>I'm working on it, <br/>will be available soon... </Typography>
        <Button className={classes.button}
            color="secondary" 
            variant="contained" 
            to="/">
                Home
        </Button>
    </div>
)};
export default WIP;
