import React, { useState } from "react";
import clsx from "clsx";
import { Button, Typography } from "@material-ui/core";
import { createStyles, makeStyles, Theme, lighten } from "@material-ui/core/styles";
import Menu from "@material-ui/icons/MenuRounded";

import NavigationModal from "../components/NavigationModal";
import Developer, { DeveloperAction, DeveloperContext } from "../components/Developer";
import RetroWaveSVG from "../components/RetrowaveSVG";
import TechnologySVG from "../components/TechnologySVG";
import Blogposts from "../components/Blogposts";
import Footer from "../components/Footer";
import PageContainer from "../components/PageContainer";
import useTypingSimulation from "../hooks/useTypeSimulation";
import { openLinkedIn, openSourceCode } from "../components/Developer/actions";

const useStyles = makeStyles((theme: Theme) => 
    createStyles({
        root: { position: 'relative' },
        center: {
            position: 'absolute',
            top: '50%',
            left: '50%',
            transform: 'translate(-50%, -50%)',
        },
        text: { color: theme.palette.text.primary, },
        title: { 
            textTransform: 'uppercase',
            marginBottom: theme.spacing(2),
        },
        section: {
            width: '100%',
            display: 'flex',
            alignItems: 'center',
            padding: theme.spacing(0, 9),
            flexWrap: 'wrap',
        },
        header: {
            width: '100%',
            display: 'flex',
            padding: theme.spacing(4, 5, 0, 8),
            [theme.breakpoints.down("xs")]: {
                padding: theme.spacing(3, 3, 0),
            }
        },
        navigationModal: {
            width: '50vw',
            height: '40%',
            backgroundColor: lighten(theme.palette.background.paper, .8),
            outline: 'none',
            borderRadius: '5px',
            overflow: 'hidden',
            boxShadow: theme.shadows[20],
        },
        headerButton: { 
            textDecoration: 'none',
            marginRight: theme.spacing(5),
            [theme.breakpoints.down("xs")]: {
                marginRight: theme.spacing(2),
            }
        },
        landing: {
            height: '90vh',
            position: 'relative',
        },
        rowRev: { flexDirection: 'row-reverse' },
        intro: {
            width: '45%',
            paddingLeft: theme.spacing(3),
            [theme.breakpoints.down("xs")]: {
                padding: 0,
                width: '100%',
                textAlign: 'center',
            },
        },
        menuIcon: { 
            marginLeft: 'auto', 
            cursor: 'pointer',
        },
        curiousButton: {
            padding: 0,
            marginTop: theme.spacing(1),
            fontSize: '1.4rem',
            textTransform: 'none',
            [theme.breakpoints.down("xs")]: {
                fontSize: '1.2rem',
            }
        },
        developer: {
            width: '52%',
            marginLeft: 'auto',
            [theme.breakpoints.down("xs")]: {
                width: '100%',
                margin: 'auto'
            },
        },
        technology: {
            paddingTop: theme.spacing(15),
            paddingBottom: theme.spacing(15),
            backgroundColor: theme.palette.background.paper,
        },
        technologySVG: { 
            width: '50%',
            [theme.breakpoints.down("xs")]: {
                width: '80%',
                margin: 'auto',
                marginBottom: theme.spacing(4),
            },
        },
        technologyText: {
            width: '45%',
            marginLeft: 'auto',
            [theme.breakpoints.down("xs")]: {
                width: '100%',
                textAlign: 'center',
            }
        },
    })
);

const IndexPage: React.FC = () => {
    const classes = useStyles();
    const [open, setOpen] = useState<boolean>(false);
    const [terminalText, startTypingSimulation] = useTypingSimulation();

    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);
    
    const handleAction = async (action: DeveloperAction) => {
        await startTypingSimulation(`${action.command} ${action.path}`);
        action.action();
    }
    
    return (
    <PageContainer seo={{ pageName: "Home" }} className={classes.root}>
        <DeveloperContext.Provider value={{
            executing: !!terminalText,
            handler: handleAction,
        }}>
            <section className={classes.landing}>
                <header className={classes.header}>
                    <Button className={classes.headerButton}
                        onClick={() => handleAction(openLinkedIn)} 
                        color="primary">
                            Contact
                    </Button>
                    <div onClick={handleOpen} className={classes.menuIcon}>
                        <Menu fontSize="large" />
                    </div>
                    <NavigationModal 
                        className={clsx(classes.navigationModal, classes.center)}
                        modalProps={{ open, onClose: handleClose }} />
                </header>
                <div className={clsx(classes.section, classes.rowRev, classes.center)}>
                    <Developer className={classes.developer} terminalText={terminalText} />
                    <div className={classes.intro}>
                        <Typography variant="h1" className={classes.title}>Web Developer</Typography>
                        <Typography variant="body1">
                            I graduated in electronics-ICT. So I have a broad background in 
                            a lot of different technologies both in hardware and in software 
                            but through my education I've become really passionate about web development. 
                            At this moment I'm mostly experienced in frontend web development 
                            using the React library.
                        </Typography>
                        <Button className={classes.curiousButton}
                            onClick={() => handleAction(openSourceCode)} 
                            color="secondary">
                                Curious how this website was made?
                        </Button>
                    </div>
                </div>
            </section>
            <RetroWaveSVG />
            <section className={clsx(classes.section, classes.technology)}>
                <TechnologySVG className={classes.technologySVG}/>
                <div className={classes.technologyText}>
                    <Typography variant="h2" className={classes.title}>Technologies</Typography>
                    <Typography variant="body1">
                        The SVG above will give you some clues to the technogies I love 
                        to use or the one’s I’m learning at te moment.  Although 
                        I am mostly experienced on the frontend part of development, my interests 
                        aren’t limited to only those technologies. I would love to become a 
                        full-stack developer.
                    </Typography>
                </div>
            </section>
            <Blogposts />
            <Footer />
        </DeveloperContext.Provider>
    </PageContainer>
)};
export default IndexPage;
