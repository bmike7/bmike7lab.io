import React from "react";
import {
    createStyles,
    makeStyles,
    Theme,
} from "@material-ui/core/styles";

import TypingSVG from "../components/WIP";
import PageContainer from "../components/PageContainer";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        typingSVG: {
            width: '40%',
            [theme.breakpoints.down('md')]: { width: '50%' }, 
            [theme.breakpoints.down('sm')]: { width: '60%' }, 
        }
    })
);

const Blogs: React.FC = () => {
    const classes = useStyles();
    return (
        <PageContainer>
            <TypingSVG className={classes.typingSVG} />
        </PageContainer>
    );
};
export default Blogs;
