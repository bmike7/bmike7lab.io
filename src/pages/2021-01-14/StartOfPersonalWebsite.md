---
title: "Start of personal website"
date: "2021-01-14"
url: ""
description: "Do you want to find out why I made this website and who this is for?"
---

# Start of my personal website

![🙏: Florian Olivo on Unsplash](./FlorianOlivoCode.jpg)

**This blogpost is written to go online along with this website. So in this one I want to talk about a few things. First a small introduction on where I stand in my current development carreer and where this website is for. Followed by the technologies I'm currently using and learning. To end with, I will reveil some of my project-plans to occupy my spare time.**

___

## Where my interests come from

During my electronics-ICT education we had a course *web frameworks* where we learned the Angular framework. At that time, with little experience with HTML, CSS and javascript, that course was hard for me at first. Later I started to play around with the React library on my own. Because of it beeing a library there were a few things I had to figure out on my own (routing is a simple example). As a result I've had a lot of "aha experiences" working with React. Those experiences is how I fell in love with development in general. Since then I've been learning a lot, reading blogs and books about coding etc.
Later I striked an internship where I worked on the code-base that also went in production. Luckily this was at a company where I was surrounded by top-level developers who made me grow so much, through code reviews and by watching how they tackle problems. Unfortunaltly a pandemic took hold over the world but I managed to make the best of it from my home office. 
My internship was coming to an end but COVID had other plans (this was the last time I mention COVID, I want the blog to have a happy note to it). So I graduated and the job search could begin. This is where my personal website comes in the picture, I want to use this one to showcase my skills and write about the things I learn while keeping myself busy with software engineering during the job search.

## The technologies I'm interested in (next to frontend development)

As mentioned I love to read blogposts about development (instead of opening social media, my go-to app is Medium) and I've went through this period where every technology grabbed my attention and I wanted to jump on it. Gladly I've become more picky but there are some that sticked with me. I think the first one is *Web Assembly*. When reading about it, this seemed like **the next big thing** in web development. Reading more and more I came across **Rust**. I think because *wasm* came from asm.js and its history at Mozilla (where Rust was designed), and so also lots of talks I watched about *wasm* had something to do with Rust.
And this last one is what I have been focussing on last couple of weeks/months. I've worked my way through "The Book" and I'm planning to work on some projects with it. Firstly to build my "Testimony"-service (kind of reviews), where at this moment a "Work in progress" page will be shown.
And I'm also looking forward to an oppertunity to use Rust for maybe optimizing something in a frontend project with wasm. 

## What's next?

![Screenshot of my beautiful improvised backlog 🚀](./NotionScreenshot.png)

As you may have guessed a following blogpost will probably be about using Rust to build the Testimony-service. I want to use a cloud provider for this one, but on a budget. From a school project I've experienced that using a database can be expensive (but possibly didn't used it in the most optimal way). So I'm thinking about storing de testimonials on a server in-memory and write a FaaS for "saving"/"logging" to a file to keep this file in sync with the data on the server. I hope this idea works out well 🤞 and also that I've tickled your interest and you can't wait for a following blogpost.

### If you made it till here, thanks for reading.
  
