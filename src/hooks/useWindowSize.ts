import React from "react";

interface Size {
    width: number,
    height: number,
};

// With refs the offset won't be updated
// 🙏: https://stackoverflow.com/questions/19014250/rerender-view-on-browser-resize-with-react
// a custom react hook in my situation is probably overkill
// but I used this one the freshen up how to write them (Hooks).
const useWindowSize = (): Size => {
    const [size, setSize] = React.useState<Size>({ width: 0, height: 0 });
    React.useLayoutEffect(() => {
        function updateSize() {
            setSize({ width: window.innerWidth, height: window.innerHeight });
        };
        updateSize();
        window.addEventListener('resize', updateSize);
        return () => window.removeEventListener('resize', updateSize);
    }, []);
    return size;
}

export default useWindowSize;
